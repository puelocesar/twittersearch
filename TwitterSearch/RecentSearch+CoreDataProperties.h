//
//  RecentSearch+CoreDataProperties.h
//  TwitterSearch
//
//  Created by Paulo Cesar on 28/10/15.
//  Copyright © 2015 Paulo Cesar. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "RecentSearch.h"

NS_ASSUME_NONNULL_BEGIN

@interface RecentSearch (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *text;
@property (nullable, nonatomic, retain) NSDate *date;

@end

NS_ASSUME_NONNULL_END
