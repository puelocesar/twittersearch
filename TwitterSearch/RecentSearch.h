//
//  RecentSearch.h
//  TwitterSearch
//
//  Created by Paulo Cesar on 28/10/15.
//  Copyright © 2015 Paulo Cesar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface RecentSearch : NSManagedObject

+ (NSArray*)lastSearchesWithContext: (NSManagedObjectContext *) context;
+ (void)createWithContext: (NSManagedObjectContext *) context andSearchText: (NSString *) searchString;

@end

NS_ASSUME_NONNULL_END

#import "RecentSearch+CoreDataProperties.h"
