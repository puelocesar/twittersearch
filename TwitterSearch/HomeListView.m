//
//  HomeListView.m
//  TwitterSearch
//
//  Created by Paulo Cesar on 28/10/15.
//  Copyright © 2015 Paulo Cesar. All rights reserved.
//

#import "HomeListView.h"

@implementation HomeListView

- (id)awakeAfterUsingCoder:(NSCoder *)aDecoder
{
    if (![self.subviews count])
    {
        NSBundle *mainBundle = [NSBundle mainBundle];
        NSArray *loadedViews = [mainBundle loadNibNamed:@"HomeListView" owner:nil options:nil];
        HomeListView *loadedView = [loadedViews firstObject];
        
        loadedView.frame = self.frame;
        loadedView.autoresizingMask = self.autoresizingMask;
        loadedView.translatesAutoresizingMaskIntoConstraints = self.translatesAutoresizingMaskIntoConstraints;
        
        // copy storyboard constraints to the new loaded view
        // ref: http://cocoanuts.mobi/2014/03/26/reusable/
        //
        for (NSLayoutConstraint *constraint in self.constraints) {
            id firstItem = constraint.firstItem;
            if (firstItem == self) {
                firstItem = loadedView;
            }
            id secondItem = constraint.secondItem;
            if (secondItem == self) {
                secondItem = loadedView;
            }
            [loadedView addConstraint:
             [NSLayoutConstraint constraintWithItem:firstItem
                                          attribute:constraint.firstAttribute
                                          relatedBy:constraint.relation
                                             toItem:secondItem
                                          attribute:constraint.secondAttribute
                                         multiplier:constraint.multiplier
                                           constant:constraint.constant]];
        }
        
        return loadedView;
    }
    return self;
}

- (void)setTitle:(NSString *)title andDatasource:(GenericDataSource*)dataSource {
    self.titleLabel.text = title;
    
    self.dataSource = dataSource;
    [self.dataSource setReuseIdentifierWithTableView:self.tableView];
    self.dataSource.delegate = self;
    
    self.tableView.dataSource = self.dataSource;
}

- (void)updateList {
    [self.dataSource getData];
}

- (void)reloadData {
    [self.tableView reloadData];
}

@end
