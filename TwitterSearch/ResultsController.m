//
//  ResultsController.m
//  TwitterSearch
//
//  Created by Paulo Cesar on 27/10/15.
//  Copyright © 2015 Paulo Cesar. All rights reserved.
//

#import "ResultsController.h"
#import "RecentSearch.h"
#import "AppDelegate.h"
@import Social;

@implementation ResultsController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.queryLabel.text = [NSString stringWithFormat:@"“%@”", self.searchString];
    
    self.tableView.delegate = self;
    
    //data source
    self.dataSource = [[SearchResultsDataSource alloc] init];
    self.dataSource.compactLayout = [self isCompactLayout];
    [self.dataSource setReuseIdentifierWithTableView:self.tableView];
    self.dataSource.delegate = self;
    self.tableView.dataSource = self.dataSource;
    
    //refresh control
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self
                            action:@selector(performSearch)
                  forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:self.refreshControl];
    [self.refreshControl beginRefreshing];
    
    //save search
    NSManagedObjectContext* context = ((AppDelegate*) [UIApplication sharedApplication].delegate).managedObjectContext;
    [RecentSearch createWithContext:context andSearchText:self.searchString];
    
    [self performSearch];
    
    //observe for share action
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(shareWithNotification:) name:@"share" object:NULL];
}

- (BOOL)isCompactLayout {
    return (self.traitCollection.horizontalSizeClass != UIUserInterfaceSizeClassRegular ||
            self.traitCollection.verticalSizeClass != UIUserInterfaceSizeClassRegular);
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([self isCompactLayout]) {
        return 120;
    }
    else {
        return 170;
    }
}

- (void)shareWithNotification:(NSNotification *)notification {
    NSString* url = [notification.userInfo objectForKey:@"url"];
    
    SLComposeViewController* controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
    [controller addURL:[NSURL URLWithString:url]];
    
    controller.completionHandler = ^(SLComposeViewControllerResult result) {
        if (result == SLComposeViewControllerResultDone) {
            
            NSString* shared = NSLocalizedString(@"Shared!", nil);
            
            //notifica sucesso
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:shared message:NULL delegate:NULL cancelButtonTitle:NULL otherButtonTitles:NULL, nil];
            [alert show];
            
            //esconde automaticamente
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [alert dismissWithClickedButtonIndex:0 animated:YES];
            });
        }
    };
    
    [self.navigationController presentViewController:controller animated:YES completion:NULL];
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (void)performSearch {
    [self.dataSource performSearch:self.searchString];
}

- (void)reloadData {
    [self.refreshControl endRefreshing];
    [self.tableView reloadData];
}

@end
