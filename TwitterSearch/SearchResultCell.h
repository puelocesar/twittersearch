//
//  SearchResultCell.h
//  TwitterSearch
//
//  Created by Paulo Cesar on 29/10/15.
//  Copyright © 2015 Paulo Cesar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TweetData.h"

@interface SearchResultCell : UITableViewCell <AvatarLoadingDelegate, UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loading;
@property (weak, nonatomic) IBOutlet UIImageView *avatarView;
@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;

@property NSString* tweetURL;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leadingConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *trailingConstraint;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIButton *actionButton;

@property float maxDelta;
@property float savedPositionX;
@property BOOL lockOnStart;

- (void)updateLayout: (BOOL) isCompact;
- (void)setupWithTweet: (TweetData*) tweet;

- (void)resetSwipeAnimated: (BOOL) animated;
- (void)rearrangeViews;

- (IBAction)shareStatus:(id)sender;

@end
