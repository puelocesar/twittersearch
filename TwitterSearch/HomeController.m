//
//  ViewController.m
//  TwitterSearch
//
//  Created by Paulo Cesar on 27/10/15.
//  Copyright © 2015 Paulo Cesar. All rights reserved.
//

#import "HomeController.h"
#import "ResultsController.h"
#import "RecentSearchesDataSource.h"
#import "TrendingDataSource.h"

static const float kBottomiPad = 198;
static const float kBottomiPhone = 60;
static const float kBottomLandscape = 20;
static const float kBottomLandscapeRegular = 60;

static NSString* kSearchSegue = @"search";

@implementation HomeController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSString* searchPlaceholder = NSLocalizedString(@"Search it", nil);
    NSString* recentSearches = NSLocalizedString(@"Recent Searches", nil);
    NSString* trendingNow = NSLocalizedString(@"Trending Now", nil);
    
    // set color of the placeholder text (not possible on IB)
    UIColor *color = [UIColor colorWithRed:0.37 green:0.62 blue:0.79 alpha:1];
    self.searchField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:searchPlaceholder
                                                                             attributes:@{NSForegroundColorAttributeName: color}];
    
    //recent searches
    [self.recentSearchesList setTitle:recentSearches andDatasource:[[RecentSearchesDataSource alloc] init]];
    [self.trendingList setTitle:trendingNow andDatasource:[[TrendingDataSource alloc] init]];
    
    self.recentSearchesList.tableView.delegate = self;
    self.trendingList.tableView.delegate = self;
    
    // keyboard events; adapt layout when user starts typing
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:self.view.window];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:self.view.window];
    
    //hide keyboard on search button
    [self.searchField addTarget:self.searchField
                         action:@selector(resignFirstResponder)
               forControlEvents:UIControlEventEditingDidEndOnExit];
}

- (void)viewDidAppear:(BOOL)animated {
    [self.recentSearchesList updateList];
    [self.trendingList updateList];
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}


// layout helpers

- (BOOL) isCompactLayout {
    return self.traitCollection.horizontalSizeClass == UIUserInterfaceSizeClassCompact;
}

- (BOOL)isLandscape {
    return ([[UIApplication sharedApplication] statusBarOrientation] == UIDeviceOrientationLandscapeLeft ||
            [[UIApplication sharedApplication] statusBarOrientation] == UIDeviceOrientationLandscapeRight);
}

// - MARK: HomeListViews delegate

// perform search with selected row on the lists
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    HomeDataSource* source = tableView.dataSource;
    self.searchString = [source getTextWithIndexPath:indexPath];
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];

    [self performSegueWithIdentifier:kSearchSegue sender:self];
}

// - MARK: Navigation

- (IBAction)executeSearch:(id)sender {
    self.searchString = [self.searchField text];
    
    if ([self.searchString length] > 0)
        [self performSegueWithIdentifier:kSearchSegue sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqual:kSearchSegue]) {
        ResultsController* results = (ResultsController*) segue.destinationViewController;
        results.searchString = self.searchString;
    }
}

- (IBAction)unwindForHome:(UIStoryboardSegue *)segue {
}

// - MARK: Keyboard events

- (void) keyboardWillShow:(NSNotification *)notification {
    
    float bottom = [[notification.userInfo objectForKey:@"UIKeyboardBoundsUserInfoKey"] CGRectValue].size.height;
    
    if ([self isLandscape]) {
        if ([self isCompactLayout])
            bottom += kBottomLandscape;
        else
            bottom += kBottomLandscapeRegular;
    }
    else if ([self isCompactLayout]) {
        bottom += kBottomiPhone;
    }
    else {
        self.leftMarginWidth.constant = 165;
        bottom += kBottomiPad;
    }
    
    self.searchFieldBottomConstraint.constant = bottom;
    
    [UIView animateWithDuration:0.2 animations:^{
        [self.view layoutIfNeeded];
        self.scrollViewContainer.alpha = 0;
    }];
}

- (void) keyboardWillHide:(NSNotification *)notification {
    
    if ([self isLandscape]) {
        if ([self isCompactLayout])
            self.searchFieldBottomConstraint.constant = kBottomLandscape;
        else
            self.searchFieldBottomConstraint.constant = kBottomLandscapeRegular;
    }
    else if ([self isCompactLayout]) {
        self.searchFieldBottomConstraint.constant = kBottomiPhone;
    }
    else {
        self.leftMarginWidth.constant = 295;
        self.searchFieldBottomConstraint.constant = kBottomiPad;
    }
    
    [UIView animateWithDuration:0.2 animations:^{
        [self.view layoutIfNeeded];
        self.scrollViewContainer.alpha = 1;
    }];
}

@end
