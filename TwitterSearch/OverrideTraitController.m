//
//  OverrideTraitController.m
//  TwitterSearch
//
//  Created by Paulo Cesar on 31/10/15.
//  Copyright © 2015 Paulo Cesar. All rights reserved.
//

#import "OverrideTraitController.h"

// Por padrão, o sistema de Size Classes não diferencia um iPad landscape de um iPad portrait
// Este é um hack para forçar o size class desejado quando o iPad estiver em modo landscape
//
// Ref: http://stackoverflow.com/questions/26633172/sizing-class-for-ipad-portrait-and-landscape-modes/30792550

@implementation OverrideTraitController

-(UITraitCollection *)overrideTraitCollectionForChildViewController:(UIViewController *)childViewController {
    if (self.view.bounds.size.width > self.view.bounds.size.height &&
        self.traitCollection.userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        
        return [UITraitCollection traitCollectionWithVerticalSizeClass:UIUserInterfaceSizeClassCompact];
    }
    else if (self.traitCollection.userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        return [UITraitCollection traitCollectionWithVerticalSizeClass:UIUserInterfaceSizeClassRegular];
    }
    else {
        return self.traitCollection;
    }
}

@end
