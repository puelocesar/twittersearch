//
//  TweetData.h
//  TwitterSearch
//
//  Created by Paulo Cesar on 29/10/15.
//  Copyright © 2015 Paulo Cesar. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AvatarLoadingDelegate;

@interface TweetData : NSObject

@property NSString* tweetID;
@property NSString* userID;
@property NSString* screenname;
@property NSString* username;
@property NSString* avatarUrl;
@property NSString* content;

@property NSString* avatarPath;
@property id<AvatarLoadingDelegate> delegate;

- (NSString*)tweetURL;

- (instancetype)initWithDict: (NSDictionary*) dict;

- (void)loadImage;
- (void)downloadImage;

@end


@protocol AvatarLoadingDelegate <NSObject>

- (void)loadingFinished: (UIImage*) image;

@end
