//
//  TweetData.m
//  TwitterSearch
//
//  Created by Paulo Cesar on 29/10/15.
//  Copyright © 2015 Paulo Cesar. All rights reserved.
//

#import "TweetData.h"

static dispatch_queue_t backgroundQueue;

@implementation TweetData

- (instancetype)initWithDict: (NSDictionary*) dict {
    
    self.tweetID = [dict objectForKey:@"id_str"];
    self.userID = [[dict objectForKey:@"user"] objectForKey:@"id_str"];
    self.username = [[dict objectForKey:@"user"] objectForKey:@"name"];
    self.screenname = [[dict objectForKey:@"user"] objectForKey:@"screen_name"];
    self.avatarUrl = [[dict objectForKey:@"user"] objectForKey:@"profile_image_url"];
    self.content = [dict objectForKey:@"text"];
    
    NSString* path = [NSString stringWithFormat:@"Documents/%@.jpg", self.userID];
    self.avatarPath = [NSHomeDirectory() stringByAppendingPathComponent:path];
    
    if (backgroundQueue == NULL) {
        backgroundQueue = dispatch_queue_create("com.paulo.tweeterAvatarLoading", NULL);
    }
    
    return [super init];
}

- (NSString *)tweetURL {
    return [NSString stringWithFormat:@"https://twitter.com/%@/status/%@", self.screenname, self.tweetID];
}

//carrega o thumbnail do disco em uma thread
//se não tiver no disco, começa o download
- (void)loadImage {
    dispatch_async(backgroundQueue, ^{
        NSData* data = [NSData dataWithContentsOfFile:self.avatarPath];

        if (data == NULL) {
            [self downloadImage];
        }
        else {
            UIImage* avatarImage = [[UIImage alloc] initWithData:data];
            
            if (self.delegate != NULL) {
                [self.delegate loadingFinished:avatarImage];
            }
        }
    });
}

// baixa o thumbnail e notifica a interface quando terminar
- (void)downloadImage {

    NSURL* url = [[NSURL alloc] initWithString:self.avatarUrl];
    NSURLSessionDataTask* task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        UIImage* avatarImage = [[UIImage alloc] initWithData:data];
        [data writeToFile:self.avatarPath atomically:YES];
        
        if (self.delegate != NULL) {
            [self.delegate loadingFinished:avatarImage];
        }
    }];
    
    [task resume];
}

@end
