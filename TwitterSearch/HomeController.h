//
//  ViewController.h
//  TwitterSearch
//
//  Created by Paulo Cesar on 27/10/15.
//  Copyright © 2015 Paulo Cesar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeListView.h"

@interface HomeController : UIViewController <UITableViewDelegate>

@property (weak, nonatomic) IBOutlet HomeListView *recentSearchesList;
@property (weak, nonatomic) IBOutlet HomeListView *trendingList;

@property (weak, nonatomic) IBOutlet UITextField *searchField;
@property (weak, nonatomic) IBOutlet UIView *scrollViewContainer;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *searchFieldBottomConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leftMarginWidth;

@property NSString* searchString;

- (void) keyboardWillShow:(NSNotification *) notification;
- (void) keyboardWillHide:(NSNotification *) notification;

- (IBAction)executeSearch:(id)sender;

@end

