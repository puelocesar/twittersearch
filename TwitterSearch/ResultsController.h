//
//  ResultsController.h
//  TwitterSearch
//
//  Created by Paulo Cesar on 27/10/15.
//  Copyright © 2015 Paulo Cesar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchResultsDataSource.h"

@interface ResultsController : UIViewController <SearchResultsDelegate, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *queryLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property UIRefreshControl* refreshControl;

@property (retain) NSString* searchString;
@property (nonatomic, retain) SearchResultsDataSource* dataSource;

- (void)performSearch;
- (void)shareWithNotification: (NSNotification*) notification;

@end
