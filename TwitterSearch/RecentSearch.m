//
//  RecentSearch.m
//  TwitterSearch
//
//  Created by Paulo Cesar on 28/10/15.
//  Copyright © 2015 Paulo Cesar. All rights reserved.
//

#import "RecentSearch.h"

@implementation RecentSearch

+ (NSArray*)lastSearchesWithContext:(NSManagedObjectContext *)context {
    NSFetchRequest* request = [[NSFetchRequest alloc] init];
    request.entity = [NSEntityDescription entityForName:@"RecentSearch" inManagedObjectContext:context];
    request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"date" ascending:NO]];
    request.fetchLimit = 5;
    
    NSError* error;
    NSArray* result = [context executeFetchRequest:request error:&error];
    
    if (error == NULL) {
        return result;
    }
    else {
        NSLog(@"%@", error.localizedDescription);
        return NULL;
    }
}

+ (void)createWithContext:(NSManagedObjectContext *)context andSearchText:(nonnull NSString *)searchString {
    
    RecentSearch* historyEntry;
    NSError* error;
    
    //se já tem, apenas atualiza a data
    NSFetchRequest* request = [[NSFetchRequest alloc] init];
    request.entity = [NSEntityDescription entityForName:@"RecentSearch" inManagedObjectContext:context];
    request.predicate = [NSPredicate predicateWithFormat:@"text = %@", searchString];
    NSArray* result = [context executeFetchRequest:request error:&error];
    
    if (result.count > 0) {
        historyEntry = [result objectAtIndex:0];
    }
    else {
        historyEntry = [NSEntityDescription insertNewObjectForEntityForName:@"RecentSearch"
                                                     inManagedObjectContext:context];
        historyEntry.text = searchString;
    }
    
    historyEntry.date = [[NSDate alloc] init];
    [context save:&error];
    
    if (error != NULL) {
        NSLog(@"%@", error.localizedDescription);
    }
}

@end
