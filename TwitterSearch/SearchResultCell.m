//
//  SearchResultCell.m
//  TwitterSearch
//
//  Created by Paulo Cesar on 29/10/15.
//  Copyright © 2015 Paulo Cesar. All rights reserved.
//

#import "SearchResultCell.h"

static const float kMaxDeltaCompact = 120;
static const float kMaxDelta = 200;
static const float kSnap = 60;

@implementation SearchResultCell

- (void)awakeFromNib {
    
    self.savedPositionX = 0;
    self.lockOnStart = true;
    
    self.avatarView.layer.masksToBounds = YES;
    
    UIPanGestureRecognizer* pan = [[UIPanGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(panThisCell:)];
    pan.delegate = self;
    [self addGestureRecognizer:pan];
    
    self.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"fundo.png"]];
    [self sendSubviewToBack:self.actionButton];
}

- (void)updateLayout:(BOOL)isCompact {
    self.maxDelta = (isCompact) ? kMaxDeltaCompact : kMaxDelta;
    self.avatarView.layer.cornerRadius = self.avatarView.frame.size.width/2;
}

- (void)setupWithTweet: (TweetData*) tweet {
    
    self.avatarView.image = NULL;
    self.loading.alpha = 1;
    [self.loading startAnimating];
    
    self.usernameLabel.text = tweet.username;
    self.contentLabel.text = tweet.content;
    
    tweet.delegate = self;
    [tweet loadImage];
    
    self.tweetURL = [tweet tweetURL];
    
    //reset swipe
    self.lockOnStart = YES;
    [self resetSwipeAnimated:NO];
}

- (void)loadingFinished:(UIImage *)image {
    dispatch_async(dispatch_get_main_queue(), ^{
        self.avatarView.image = image;
        self.loading.alpha = 0;
    });
}

// -MARK: slide cell

// inspiration: http://www.raywenderlich.com/62435/make-swipeable-table-view-cell-actions-without-going-nuts-scroll-views

- (void)panThisCell:(UIPanGestureRecognizer *)recognizer {
    
    CGPoint currentPoint = [recognizer translationInView:self.containerView];
    
    switch (recognizer.state) {
        case UIGestureRecognizerStateBegan:
            break;
            
        case UIGestureRecognizerStateChanged: {
            
            CGFloat deltaX = currentPoint.x + self.savedPositionX;
            
            //não deixa ir para o lado errado
            if (deltaX < 0) deltaX = 0;
            
            //atualiza constraints
            self.leadingConstraint.constant = deltaX;
            self.trailingConstraint.constant = -deltaX;
            [self layoutIfNeeded];
            
            //quando soltar, trava no começo ou na posição final?
            self.lockOnStart = deltaX <= self.maxDelta-kSnap;
        }
        break;
            
        case UIGestureRecognizerStateEnded:
            [self resetSwipeAnimated:YES];
            break;
            
        case UIGestureRecognizerStateCancelled:
            [self resetSwipeAnimated:YES];
            break;
            
        default:
            break;
    }
}

- (void)rearrangeViews {
    if (self.lockOnStart)
        [self sendSubviewToBack:self.actionButton];
    else
        [self sendSubviewToBack:self.contentView];
}

- (void)resetSwipeAnimated:(BOOL)animated {
    self.savedPositionX = self.lockOnStart ? 0 : self.maxDelta;
    
    self.leadingConstraint.constant = self.savedPositionX;
    self.trailingConstraint.constant = -self.savedPositionX;
    
    if (animated)
        [UIView animateWithDuration:0.1 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            [self layoutIfNeeded];
        } completion:^(BOOL FINISHED) {
            [self rearrangeViews];
        }];
    else {
        [self layoutIfNeeded];
        [self rearrangeViews];
    }
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return true;
}

// -MARK: actions

- (IBAction)shareStatus:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"share" object:self userInfo:@{@"url": self.tweetURL}];
}

@end
