//
//  SearchResultsData.h
//  TwitterSearch
//
//  Created by Paulo Cesar on 27/10/15.
//  Copyright © 2015 Paulo Cesar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GenericDataSource.h"

@interface SearchResultsDataSource : GenericDataSource

@property BOOL compactLayout;
- (void)performSearch: (NSString*) searchString;

@end