//
//  HomeListCell.h
//  TwitterSearch
//
//  Created by Paulo Cesar on 29/10/15.
//  Copyright © 2015 Paulo Cesar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeListCell : UITableViewCell

@property UILabel* label;

- (void)setText: (NSString*) text;

@end
