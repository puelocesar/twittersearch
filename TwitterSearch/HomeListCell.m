//
//  HomeListCell.m
//  TwitterSearch
//
//  Created by Paulo Cesar on 29/10/15.
//  Copyright © 2015 Paulo Cesar. All rights reserved.
//

#import "HomeListCell.h"

@implementation HomeListCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        
        self.label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        
        self.label.font = [UIFont fontWithName:@"HelveticaNeue" size:17];
        self.label.textColor = [UIColor colorWithRed:0.41 green:0.41 blue:0.44 alpha:1];
        
        [self setBackgroundView:self.label];
    }
    return self;
}

- (void)setText:(NSString *)text {
    self.label.text = text;
}

@end
