//
//  RecentSearchesData.m
//  TwitterSearch
//
//  Created by Paulo Cesar on 28/10/15.
//  Copyright © 2015 Paulo Cesar. All rights reserved.
//

#import "RecentSearchesDataSource.h"
#import "AppDelegate.h"
#import "RecentSearch.h"
#import "HomeListCell.h"

@implementation RecentSearchesDataSource

- (void)getData {
    NSManagedObjectContext* context = ((AppDelegate*) [UIApplication sharedApplication].delegate).managedObjectContext;
    self.results = [RecentSearch lastSearchesWithContext:context];
    [self.delegate reloadData];
}

- (NSString*)getTextWithIndexPath:(NSIndexPath *)indexPath {
    RecentSearch* historyEntry = [self.results objectAtIndex:indexPath.row];
    return historyEntry.text;
}

@end
