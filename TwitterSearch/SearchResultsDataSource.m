//
//  SearchResultsData.m
//  TwitterSearch
//
//  Created by Paulo Cesar on 27/10/15.
//  Copyright © 2015 Paulo Cesar. All rights reserved.
//

#import "SearchResultsDataSource.h"
#import <TwitterKit/TwitterKit.h>
#import "SearchResultCell.h"
#import "TweetData.h"

@implementation SearchResultsDataSource

- (void)setReuseIdentifierWithTableView:(UITableView *)tableView {
    self.reuseIdentifier = @"SearchResult";
    UINib* nib = [UINib nibWithNibName:@"SearchResultCell" bundle:nil];
    [tableView registerNib:nib forCellReuseIdentifier:self.reuseIdentifier];
}

- (void)performSearch:(NSString *)searchString {
    
    self.results = NULL;
    
    NSString *userID = [Twitter sharedInstance].sessionStore.session.userID;
    TWTRAPIClient* client = [[TWTRAPIClient alloc] initWithUserID:userID];
    NSString* twitterQuery = @"https://api.twitter.com/1.1/search/tweets.json";
    NSDictionary* params = @{@"q" : searchString};
    NSError* clientError;
    
    NSURLRequest *request = [[[Twitter sharedInstance] APIClient] URLRequestWithMethod:@"GET" URL:twitterQuery parameters:params error:&clientError];
    
    if (request) {
        [client sendTwitterRequest:request completion:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
            if (data) {
                NSError *jsonError;
                NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError];
                
                NSArray* tweets = [json objectForKey:@"statuses"];
                self.errorMessage = NULL;
                
                NSMutableArray* result = [[NSMutableArray alloc] initWithCapacity:tweets.count];
                for (NSDictionary* tweetDict in tweets) {
                    [result addObject:[[TweetData alloc] initWithDict:tweetDict]];
                }
                self.results = result;
            }
            else {
                self.errorMessage = connectionError.localizedDescription;
            }
            
            if (self.delegate != NULL) {
                [self.delegate reloadData];
            }
        }];
    }
    else {
        self.errorMessage = clientError.localizedDescription;
        
        if (self.delegate != NULL) {
            [self.delegate reloadData];
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SearchResultCell *cell = [tableView dequeueReusableCellWithIdentifier:self.reuseIdentifier forIndexPath:indexPath];
    
    if (self.errorMessage != NULL) {
        cell.textLabel.text = self.errorMessage;
    }
    else if (self.results != NULL) {
        TweetData *tweet = [self.results objectAtIndex:indexPath.row];
        [cell setupWithTweet:tweet];
        [cell updateLayout:self.compactLayout];
    }
    
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}


@end


