//
//  HomeListView.h
//  TwitterSearch
//
//  Created by Paulo Cesar on 28/10/15.
//  Copyright © 2015 Paulo Cesar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GenericDataSource.h"

@interface HomeListView : UIView <SearchResultsDelegate>

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (retain) GenericDataSource* dataSource;

- (void)setTitle: (NSString*) title andDatasource: (GenericDataSource*) dataSource;
- (void)updateList;

@end
