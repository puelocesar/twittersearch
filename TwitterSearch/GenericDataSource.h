//
//  GenericSearchData.h
//  TwitterSearch
//
//  Created by Paulo Cesar on 28/10/15.
//  Copyright © 2015 Paulo Cesar. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SearchResultsDelegate;

@interface GenericDataSource : NSObject <UITableViewDataSource>

@property (retain) NSArray* results;
@property id<SearchResultsDelegate> delegate;
@property NSString* errorMessage;
@property (retain) NSString* reuseIdentifier;

- (void)setReuseIdentifierWithTableView: (UITableView*) tableView;
- (void)getData;

@end



@protocol SearchResultsDelegate <NSObject>
- (void)reloadData;
@end