//
//  TrendingData.m
//  TwitterSearch
//
//  Created by Paulo Cesar on 28/10/15.
//  Copyright © 2015 Paulo Cesar. All rights reserved.
//

#import "TrendingDataSource.h"
#import <TwitterKit/TwitterKit.h>
#import "HomeListCell.h"

@implementation TrendingDataSource

- (void)getData {
    NSString *userID = [Twitter sharedInstance].sessionStore.session.userID;
    TWTRAPIClient* client = [[TWTRAPIClient alloc] initWithUserID:userID];
    NSString* twitterQuery = @"https://api.twitter.com/1.1/trends/place.json";
    NSDictionary* params = @{@"id" : @"1"};
    NSError* clientError;
    
    NSURLRequest *request = [[[Twitter sharedInstance] APIClient] URLRequestWithMethod:@"GET" URL:twitterQuery parameters:params error:&clientError];
    
    if (request) {
        [client sendTwitterRequest:request completion:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
            if (data) {
                NSError *jsonError;
                NSArray *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError];
                
                self.results = [[json objectAtIndex:0] objectForKey:@"trends"];
                self.errorMessage = NULL;
            }
            else {
                self.errorMessage = connectionError.localizedDescription;
            }
            
            if (self.delegate != NULL) {
                [self.delegate reloadData];
            }
        }];
    }
    else {
        self.errorMessage = clientError.localizedDescription;
        
        if (self.delegate != NULL) {
            [self.delegate reloadData];
        }
    }
}

- (NSString*)getTextWithIndexPath:(NSIndexPath *)indexPath {
    return [[self.results objectAtIndex:indexPath.row] objectForKey:@"name"];
}

@end
