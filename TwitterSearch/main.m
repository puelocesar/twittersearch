//  main.m
//  TwitterSearch
//
//  Created by Paulo Cesar on 27/10/15.
//  Copyright © 2015 Paulo Cesar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
