//
//  RecentSearch+CoreDataProperties.m
//  TwitterSearch
//
//  Created by Paulo Cesar on 28/10/15.
//  Copyright © 2015 Paulo Cesar. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "RecentSearch+CoreDataProperties.h"

@implementation RecentSearch (CoreDataProperties)

@dynamic text;
@dynamic date;

@end
