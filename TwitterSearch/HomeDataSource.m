//
//  HomeDataSource.m
//  TwitterSearch
//
//  Created by Paulo Cesar on 29/10/15.
//  Copyright © 2015 Paulo Cesar. All rights reserved.
//

#import "HomeDataSource.h"
#import "HomeListCell.h"

@implementation HomeDataSource

- (NSString*)getTextWithIndexPath: (NSIndexPath*) indexPath {
    return NULL;
}

- (void)setReuseIdentifierWithTableView:(UITableView *)tableView {
    self.reuseIdentifier = @"homeListCell";
    [tableView registerClass:[HomeListCell class] forCellReuseIdentifier:self.reuseIdentifier];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    HomeListCell* cell = [tableView dequeueReusableCellWithIdentifier:self.reuseIdentifier forIndexPath:indexPath];
    
    if (self.results == NULL && self.errorMessage != NULL) {
        [cell setText:self.errorMessage];
    }
    else {
        [cell setText:[self getTextWithIndexPath:indexPath]];
    }
    
    return cell;
}

@end
