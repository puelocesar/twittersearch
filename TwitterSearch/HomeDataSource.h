//
//  HomeDataSource.h
//  TwitterSearch
//
//  Created by Paulo Cesar on 29/10/15.
//  Copyright © 2015 Paulo Cesar. All rights reserved.
//

#import "GenericDataSource.h"

@interface HomeDataSource : GenericDataSource

- (NSString*)getTextWithIndexPath: (NSIndexPath*) indexPath;

@end
