//
//  GenericSearchData.m
//  TwitterSearch
//
//  Created by Paulo Cesar on 28/10/15.
//  Copyright © 2015 Paulo Cesar. All rights reserved.
//

#import "GenericDataSource.h"

@implementation GenericDataSource

- (void)setReuseIdentifierWithTableView:(UITableView *)tableView {
}

- (void)getData {
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    return NULL;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.results == NULL && self.errorMessage == NULL) {
        return 0;
    }
    else if (self.results == NULL && self.errorMessage != NULL) {
        return 1;
    }
    else {
        return self.results.count;
    }
}


@end
